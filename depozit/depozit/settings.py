"""
Django settings for depozit project.

Generated by 'django-admin startproject' using Django 3.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from os import path, getenv
from pathlib import Path
from datetime import date


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
TEMPLATES_DIR = path.join(BASE_DIR, 'my_app/templates/my_app')
MEDIA_DIR = path.join(BASE_DIR, 'media')
LOGS_DIR = path.join(BASE_DIR, 'logs')
STATIC_DIR = path.join(BASE_DIR, 'my_app/static')
CRISPY_TEMPLATE_PACK = 'bootstrap4'
LOGIN_REDIRECT_URL = '/first_page'


SESSION_COOKIE_SECURE = True
SESSION_COOKIE_AGE = 7200 # 1h
# PHONENUMBER_DB_FORMAT = "E642"



# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-j^qr*5btcw#r#28kfk&l4jnzduk@2foui%s^8)=ok&h*rcw^vu'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'my_app',
    'crispy_forms',
    'django_prometheus',
    # 'phonenumber_field',
    'mptt',
    'sweetify'
]
SWEETIFY_SWEETALERT_LIBRARY = 'sweetalert2'


MIDDLEWARE = [
    'django_prometheus.middleware.PrometheusBeforeMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_prometheus.middleware.PrometheusAfterMiddleware',
]
X_FRAME_OPTIONS = 'SAMEORIGIN'
ROOT_URLCONF = 'depozit.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATES_DIR ,],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'depozit.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'new',
#         'USER': 'root',
#         'PASSWORD': 'cvscvs',
#         'HOST': '127.0.0.1',
#         'PORT': 3307,
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.mysql',
        'NAME': 'depozit',
        'USER': 'root',
        'PASSWORD': 'cvscvs',
        'HOST': '127.0.0.1',
        'PORT': 3307,
    }
}
date_today = date.today().strftime("%Y-%m-%d")
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '{asctime} {levelname} [{module}:{lineno}] - {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },

    'handlers': {

        'sql': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': path.join(BASE_DIR, f'logs/{date_today}_sql.log'),
            'formatter': 'verbose'
        },
        'info': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': path.join(BASE_DIR, f'logs/{date_today}_info.log'),
            'formatter': 'verbose'
        },
        'request': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': path.join(BASE_DIR, f'logs/{date_today}_request.log'),
            'formatter': 'verbose'
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': path.join(BASE_DIR, f'logs/{date_today}_debug.log'),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['sql'],
            'level': getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'propagate': False,
        },
        'django.request': {
            'handlers': ['request'],
            'level': getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'propagate': False,
        },
        'my_app.views' : {
            'handlers': ['info'],
            'level': getenv('DJANGO_LOG_LEVEL', 'INFO'),
            'propagate': False,
        }
    },
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    # },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Bucharest'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = [STATIC_DIR, ]

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# MEDIA
MEDIA_ROOT = MEDIA_DIR
MEDIA_URL = '/media/'