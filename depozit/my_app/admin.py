from django.contrib import admin
from .models import Product, Category
from mptt.admin import MPTTModelAdmin

admin.site.register(Category, MPTTModelAdmin)

admin.site.register(Product)
# Register your models here.
