from django import forms
from django.forms import fields
from my_app.models import Product, Category, UserProfile
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
#from phonenumber_field.formfields import PhoneNumberField


class ProductForm(forms.ModelForm):
    
    class Meta():
        model = Product
        fields = ('name', 'price', 'description', 'items', 'product_pic')

class CategoryForm(forms.ModelForm):
    class Meta():
        model = Category
        fields = ('name', 'parent')

class RegisterForm(UserCreationForm):
    email = forms.EmailField()
    # phone = PhoneNumberField()

    class Meta():
        model = User
        User.is_superuser=False
        fields = ('username', 'email', 'password1', 'password2')
    
    # def save(self, commit=True):
    #     user = super(RegisterForm, self).save(commit=False)
    #     user.email = self.cleaned_data["email"]
    #     user.phone = self.cleaned_data["phone"]
    #     if commit:
    #         user.save()
    #     return user
class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('user','avatar')

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']
        return avatar

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=100, help_text='Last Name')
    last_name = forms.CharField(max_length=100, help_text='Last Name')
    email = forms.EmailField(max_length=150, help_text='Email')
    avatar = forms.ImageField(required=False)


    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'avatar')