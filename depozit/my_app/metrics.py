from depozit.settings import LOGS_DIR
from prometheus_client import Counter, Histogram, Gauge, Summary, Info
import threading
import os
import time
os.environ.setdefault('DJANGO_SETTINGS_MODULE','depozit.settings')


product_added = Counter('product_added', 'number of products added')
thread_counter = Summary('thread', 'This is the thread active counter')
logs_size = Gauge('logs', 'This is the logs size')
info = Info('depozit', 'Get some details')

def monitorize():
    info.info({'PID': str(os.getpid()), 'owner_ID': str(os.getuid())})
    thread_counter.observe(threading.active_count())
    while True:
        tmp = sum(d.stat().st_size for d in os.scandir(LOGS_DIR) if d.is_file())
        logs_size.set(tmp)
        time.sleep(15)

