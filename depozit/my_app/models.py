from email.policy import default
from django.db import models
from django.contrib.auth.models import Group, User
from django.urls import reverse
from django_prometheus.models import ExportModelOperationsMixin
from mptt.models import MPTTModel, TreeForeignKey
from django.db.models.signals import post_save
from django.dispatch import receiver
from PIL import Image


class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='profile_pic')
    def __str__(self):
        return self.user


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=150)
    avatar = models.ImageField(default='default.png', upload_to='profile_pic')

    def __str__(self):
        return self.user.username
    # Override the save method of the model
    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.avatar.path) # Open avatar
        
        # resize avatar
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size) # Resize avatar
            img.save(self.avatar.path) # Save it again and override the larger avatar

@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

class Category(MPTTModel):
    __metaclass__ = ExportModelOperationsMixin
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, default=None)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    name = models.CharField(max_length=200)
    class MPTTMeta:
        order_insertion_by = ['name']
    def __str__(self):
        return self.name

class Product( models.Model):
    __metaclass__ = ExportModelOperationsMixin
    name = models.CharField(max_length=70)
    price = models.IntegerField()
    description = models.TextField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=None)
    product_pic = models.ImageField(upload_to='product_pic', blank=True, default=None)
    items = models.PositiveIntegerField(default=0)
    def __str__(self):                      
        return self.name
    def get_absolute_url(self):
        return reverse("my_app:product", kwargs={'pk':self.pk})


