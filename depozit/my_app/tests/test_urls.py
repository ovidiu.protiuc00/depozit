from django.test import TestCase
from django.urls import reverse, resolve
from my_app import views
from my_app.models import Category, Product
from django.contrib.auth.models import User
from random import choice
from django.shortcuts import get_object_or_404, render

class TestUrls(TestCase):
    def test_login_is_resolved(self):
        url = reverse('my_app:index')
        self.assertEquals(resolve(url).func, views.index)

    def test_add_product_is_resolved(self):
        url = reverse('my_app:add_product')
        self.assertEquals(resolve(url).func, views.add_product)

    def test_products_is_resolved(self):
        url = reverse('my_app:products')
        self.assertEquals(resolve(url).func, views.products)
    
 #   def test_login_is_resolved(self):
  #      p = Category.objects.get_or_create(user = 'cat', name = 'ceva')
  #      url = reverse('my_app:categorys', args=[p.pk] )
   #     self.assertEquals(resolve(url).func, views.categorys)
 