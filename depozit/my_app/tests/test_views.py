from django.http import request
from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from my_app.models import Category, Product
from my_app import views
from django.contrib.auth.models import User
import tempfile



class TestViews(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='jacob', email='jacob@yahoo.com',
          password='top_secret')
        self.category = Category.objects.create(user = self.user, name = 'Auto', id_parent = 0)

        self.image = tempfile.NamedTemporaryFile(suffix=".jpg").name

    
    def test_add_category_POST(self):
        request = self.factory.post(reverse('my_app:add_category'), 
          {'name': 'Drinks', 'id_parent': 0})
        request.user = self.user
        response = views.add_category(request)

        added_category = Category.objects.get(pk=2)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(added_category.name, 'Drinks')
        self.assertEquals(added_category.id_parent, 0)

    def test_index_GET(self):
        request = self.factory.get(reverse('my_app:index'))
        request.user = self.user
        response = views.index(request)

        self.assertEquals(response.status_code, 200)

    def test_add_product_POST(self):
        request = self.factory.post(reverse('my_app:add_product'),
        {
          'name': 'Bmw',
          'price': 10000,
          'description': 'This is the best Bmw.',
          'id_category': self.category.id, # you should put the id, for foreign keys
          'product_pic': '/tmp/temp.jpeg',
          'items': 14,
        })
        request.user = self.user
        response = views.add_product(request)
        response.client = Client()

        added_product = Product.objects.get(pk=1)

        self.assertEquals(response.status_code, 302)
        self.assertEquals(added_product.name, 'Bmw')
        self.assertEquals(added_product.price, 10000)
        self.assertEquals(added_product.description, 'This is the best Bmw.')
        self.assertEquals(added_product.id_category.id, self.category.id)
        self.assertEquals(added_product.items, 14)
        
        self.assertRedirects(response, reverse('my_app:products_of_category', args=[self.category]),
          status_code=302, target_status_code=302)


