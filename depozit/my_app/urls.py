"""depozit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from my_app import views
from django.conf import settings
from django.conf.urls.static import static


app_name='my_app'
urlpatterns = [
    path('', views.index, name='index'),
    path('first_page/', views.first_page, name='first_page'),
    path('sign_up', views.sign_up, name='sign_up'),
    path('products/', views.products, name='products'),
    path('products/<int:pk>', views.product, name='product'),
    path('products/<int:pk>/update', views.ProductUpdateView.as_view(), name='update_product'),
    path('products/<int:pk>/delete', views.delete_product, name='delete_product'),
    path('categorys/', views.categorys, name='categorys'),
    path('categorys/<str:pk>', views.products_of_category, name='products_of_category'),
    path('categorys/<str:pk>/add_product/', views.add_product, name='add_product'),
    path('add_category/', views.add_category, name='add_category'),
    path('account', views.account, name='account'),
    # path('add_user', views.add_user, name='add_user'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
