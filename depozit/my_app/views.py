from my_app.models import Category, Product
from django.shortcuts import redirect, render
from my_app.forms import CategoryForm, ProductForm, RegisterForm, SignUpForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
from django.views.generic import UpdateView
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from my_app import metrics
import logging
from django.contrib import messages
import threading
import sweetify
from django.http import HttpResponse
from django.views.decorators.clickjacking import xframe_options_deny
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.contrib.auth.models import Group


thread1 = threading.Thread(target= metrics.monitorize, daemon=True)
thread1.start()


logger = logging.getLogger('my_app.views')

logger.info('Depozit Started!')

def index(request):
    if request.user.is_authenticated:
        a = Group.objects.raw("select * from auth_user_groups aug where user_id = " + str(request.user.id) + ";")
        for i in a:
            request.session['group_id'] = i.group_id
    return render(request, "index.html")

@login_required
def first_page(request):
    if request.user.is_authenticated:
        a = Group.objects.raw("select * from auth_user_groups aug where user_id = " + str(request.user.id) + ";")
        for i in a:
            request.session['group_id'] = i.group_id
    return render(request, "first_page.html")

@login_required
def add_product(request, pk):
    if request.method == 'POST':
        product_form = ProductForm(data = request.POST)
        if product_form.is_valid():
            product = product_form.save(commit=False)
            if 'product_pic' in request.FILES:
                product.product_pic = request.FILES['product_pic']
            product.category = get_object_or_404(Category, pk = pk)
            product.save()
            metrics.product_added.inc()
            logger.info(f'Succesfully Added Product: ')
            sweetify.success(request, f'Successfully added product {product.name}!', position='top-right', timer=2000)
            return redirect(reverse('my_app:products_of_category', args=[product.category.id]))
        else:
            sweetify.error(request, title='Oops...', icon= 'error', text= 'Something went wrong!', footer= "<a href='{%url 'my_app:index'%}'>Why do I have this issue?</a>", persistent='Ok')
            logger.error(product_form.errors)
    else:
        product_form = ProductForm()
    return render(request, 'add_product.html', {'product_form' : product_form})

def sign_up(request):
    if request.method == 'POST':
        form = SignUpForm(data = request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.email = form.cleaned_data.get('email')
            if 'avatar' in request.FILES:
                    user.profile.avatar = request.FILES['avatar']
            user.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            Group.objects.get_or_create(name=username)
            group = Group.objects.get(name=username) 
            group.user_set.add(user)
            logger.info(f'New group:{group.id}')
            sweetify.success(request, f'Welcome {username}!', position='top-right', timer=1500)
            return redirect('my_app:index')
        else:
            logger.error(form.errors)
    else:
        form = SignUpForm()
    return render(request, 'sign_up.html', {'form': form})

@login_required
def delete_product(request, pk):
    try:
        product = get_object_or_404(Product, pk = pk)
        product.delete()
        return redirect(reverse('my_app:products_of_category', args=[product.category.id]))
    except:
        logger.error(f"The product with pk = {pk} , is not valid!")
    return render(request, 'products.html')


def products(request):
    products = Category.objects.raw("SELECT p.* FROM my_app_product p inner join "\
      "my_app_subcategory s on s.id=p.subcategory_id inner join my_app_category c " \
      "on s.category_id = c.id where s.id = 1 and c.user_id = " + str(request.user.id) + ";")
    print()
    return render(request, 'products.html', {'products' : products})

@login_required
def product(request, pk):
    product = get_object_or_404(Product, pk = pk)
    return render(request, 'product.html', {'product' : product})

@login_required
def categorys(request):
    users_group = Group.objects.raw("SELECT * from auth_user_groups WHERE group_id = " + str(request.session['group_id']) + ";")
    tmp = []
    for i in users_group:
        tmp.append(i.user_id)
    category = Category.objects.filter(user__id__in=tmp)
    return render(request, "categorys.html", {'categorys': category})


@login_required
def products_of_category(request, pk):
    group_id = str(request.session['group_id'])
    users_group = Group.objects.raw("SELECT * from auth_user_groups WHERE group_id = " + group_id + ";")
    tmp = []
    for i in users_group:
        tmp.append(i.user_id)
    category = Category.objects.filter(user__id__in=tmp)
    
    products_of_category = Product.objects.raw("SELECT DISTINCT p.* FROM my_app_product p "\
    "inner join my_app_category c on c.id = p.category_id "\
    "INNER JOIN auth_user_groups aug on c.group_id = aug.group_id "\
    "where c.id =" + str(pk) + "  and aug.group_id =" + group_id + ";")
    return render(request, 'products_of_category.html', 
    {
      'products_of_category':products_of_category,
      'categorys': category,
      'current_category': pk,
    })

@login_required
def add_category(request):
    if request.method == 'POST':
        category_form = CategoryForm(data = request.POST)
        if category_form.is_valid():
            category = category_form.save(commit=False)
            category.user = request.user
            category.group = Group.objects.get(id = request.session['group_id'])
            category.save()
            logger.info(f'Succesfully Added Category: ')
            return redirect(reverse('my_app:categorys'))
        else:
            logger.error("Category invalid; " + category_form.errors)
    else:
        logger.error("Bad HTTP request, should be POST!")
        category_form = CategoryForm()
    return render(request, "add_category.html", {'category_form':category_form})

class ProductUpdateView(LoginRequiredMixin,UpdateView):
    login_url = '/login/'
    redirect_field_name = 'product.html'
    template_name = "product_update.html"
    template_name_suffix = '_update'
    form_class = ProductForm

    model = Product

@xframe_options_sameorigin
@login_required
def account(request, super_user = None):
    #user = request.user
    #form2 = UserProfile()
    if request.method == "POST":
        form = RegisterForm(data = request.POST)
        #form2 = UserProfile(data = request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            if super_user is None:
                group = Group.objects.get(name=request.user.username) 
                group.user_set.add(user)
            else:
                group = Group.objects.get(name=super_user) 
                group.user_set.add(user)
            return redirect('my_app:index')
        else:
            logger.error("Category invalid; " + str(form.errors))
    else:
        form = RegisterForm()

    return render(request, "account.html", {"form":form})

# @login_required
# def account(request):
#     return render(request, "account.html")




