import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','depozit.settings')

import django
django.setup()

import random
from my_app.models import Product, Category, Profile, UserProfile
from my_app.forms import SignUpForm, RegisterForm
from django.contrib.auth.models import User
from faker import Faker
from random import choice
from django.shortcuts import get_object_or_404, render
import glob, os
os.chdir("media/product_pic/")

fake_gen = Faker()




products = [
  "Daewoo",
  "Daihatsu",
  "Dodge",
  "Donkervoort",
  "DS",
  "Ferrari",
  "Fiat",
  "Fisker",
  "Ford",
  "Honda",
  "Hummer",
  "Hyundai",
]


def populate(N, user):
    image_list = []
    category = [
    "Abarth",
    "Alfa Romeo",
    "Aston Martin",
    "Audi",
    "Bentley",
    "BMW",
    "Bugatti",
    "Cadillac",
    "Chevrolet",
    "Chrysler",
    "Citroën",
    "Dacia"
    ]
    for product_pic in glob.glob("*.jpeg"):
        image_list.append(product_pic)
    bmw = Category.objects.create(name="BMW Bavaria", user_id=1, group_id = 1)
    bmw_x7 = Category.objects.create(name="BMW X7", parent=bmw, user_id=1, group_id = 1)
    bmw_x3 = Category.objects.create(name="BMW X3", parent=bmw, user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=bmw_x7,user_id=1, group_id = 1)
    janta_x7 = Category.objects.create(name="Jante", parent=roata_x7,user_id=1, group_id = 1)
    roata_x3 = Category.objects.create(name="Anvelope", parent=bmw_x3,user_id=1, group_id = 1)
    janta_x3 = Category.objects.create(name="Jante", parent=roata_x3,user_id=1, group_id = 1)

    audi = Category.objects.create(name="Audi", user_id=1, group_id = 1)
    audi_a6 = Category.objects.create(name="Audi A6", parent=audi, user_id=1, group_id = 1)
    roata_a6 = Category.objects.create(name="Anvelope", parent=audi_a6, user_id=1, group_id = 1)
    janta = Category.objects.create(name="Jante", parent=roata_a6, user_id=1, group_id = 1)

    audi = Category.objects.create(name="Volkswagen", user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=audi,user_id=1, group_id = 1)
    audi = Category.objects.create(name="Bentley", user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=audi,user_id=1, group_id = 1)

    audi = Category.objects.create(name="Abarth", user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=audi,user_id=1, group_id = 1)

    audi = Category.objects.create(name="Cadillac", user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=audi,user_id=1, group_id = 1)
    audi = Category.objects.create(name="Dodge", user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=audi,user_id=1, group_id = 1)
    audi = Category.objects.create(name="Honda", user_id=1, group_id = 1)
    roata_x7 = Category.objects.create(name="Anvelope", parent=audi,user_id=1, group_id = 1)

    
    for i in range(N):
        name = random.choice(products)
        price = random.randrange(3000, 25000, 500)
        description = fake_gen.unique.text()

        pks = Category.objects.values_list('pk', flat=True)
        random_pk = choice(pks)
        category = Category.objects.get(pk=random_pk)

 
        product_pic = random.choice(image_list)
        product_pic = "/product_pic/" + product_pic
        items = random.randint(1, 20)

        Product.objects.get_or_create(name = name, price = price, 
        description = description, category = category, product_pic = product_pic, items = items)

if __name__ == '__main__':
    # Profile.objects.get_or_create(user = "marius", first_name = "Nutu", last_name = "Marius", email = "nutu.marius@yahoo.com", avatar = '/profile_pic/default.png', password1= "cvscvs", password2 = "cvscvs")
    
    # Profile.objects.get_or_create(user = "marius1", first_name = "Nutu", last_name = "Marius", email = "nutu.marius@yahoo.com", avatar = '/profile_pic/default.png')
    # Profile.objects.get_or_create(user = "marius2", first_name = "Nutu", last_name = "Marius", email = "nutu.marius@yahoo.com", avatar = '/profile_pic/default.png')
    # Profile.objects.get_or_create(user = "oprotiuc", first_name = "Nutu", last_name = "Marius", email = "oprotiuc@yahoo.com", avatar = '/profile_pic/default.png')
    # Profile.objects.get_or_create(user = "oprotiuc1", first_name = "Nutu", last_name = "Marius", email = "oprotiuc@yahoo.com", avatar = '/profile_pic/default.png')
    # Profile.objects.get_or_create(user = "oprotiuc2", first_name = "Nutu", last_name = "Marius", email = "oprotiuc@yahoo.com", avatar = '/profile_pic/default.png')

    print('Populating script!')

    user = get_object_or_404(User, pk = 1)
    populate(200, user)
    # user = get_object_or_404(User, pk = 2)
    # populate(100, user)
    # user = get_object_or_404(User, pk = 3)
    # populate(100, user)
    print("Complete")
