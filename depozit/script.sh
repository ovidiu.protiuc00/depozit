(docker-compose down -t0 && docker-compose up -d) &&
sleep 15 &&
python3 manage.py makemigrations &&
python3 manage.py migrate &&
# Import into DB
mysql -h 127.0.0.1 -pcvscvs -u root -P3307 depozit < config.sql &&
# export from DB
# mysqldump --column-statistics=0 -h 127.0.0.1 -pcvscvs -u root -P3307 depozit > config.sql
# Create super users
#echo "from django.contrib.auth.models import User; User.objects.create_superuser('oprotiuc', 'oprotiuc@quicksuitetrading.com', 'cvscvs')" | python3 manage.py shell &&
#echo "from django.contrib.auth.models import User; User.objects.create_superuser('marius', 'oprotiuc@quicksuitetrading.com', 'cvscvs')" | python3 manage.py shell &&
#echo "from django.contrib.auth.models import User; User.objects.create_superuser('andrei', 'oprotiuc@quicksuitetrading.com', 'cvscvs')" | python3 manage.py shell 
python3 populate.py

